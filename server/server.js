const dotenv = require('./env');
const express = require('express');
const compression = require('compression');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const routes = require('./routes');
const databaseSetup = require('./db/databaseSetup');
databaseSetup.connectToDB();

const app = express();;
app.use(compression());
app.use(cookieParser());
app.use(bodyParser.json()); // For json
app.use(bodyParser.urlencoded({ // For x-www-form-urlencoded
  extended: true
}));

routes.addRoutes(app);

app.listen(process.env.PORT || 3001, () => {
  console.log('Express server started on port ', process.env.PORT || 3001);
});

process.on('uncaughtException', (err) => {
  console.log('uncaughtException');
  console.log('err', err);
});
