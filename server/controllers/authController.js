const User = require('../models/User');

exports.register = async (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;

  console.log('email', email);
  console.log('password', password);

  // Validate later

  const validationErrors = [];

  if (!email) {
    validationErrors.push({
      type: 'VALIDATION_ERROR',
      field: 'email',
      code: 'EMAIL_REQUIRED',
      message: 'Email is required'
    })
  }

  if (!password) {
    validationErrors.push({
      type: 'VALIDATION_ERROR',
      field: 'password',
      code: 'PASSWORD_REQUIRED',
      message: 'Password is required'
    })
  }

  if (validationErrors.length) {
    res.status(422).send({
      error: true,
      errors: validationErrors
    });
    return;
  }



  // Allow user to register if "email" has not been used
  try {
    const existingUser = await User.findOne({ email });

    if (existingUser) {
      // User exists!!
      res.status(422).send({
        error: true,
        errors:[{
          type: 'GLOBAL_ERROR',
          field: '', // no specific field
          code: 'REGISTRATION_USER_EXISTS',
          message: 'User already exists'
        }]
      })

      return;
    }

    // New user
    const newUser = new User({
      email,
      password
    });

    const savedUser = await newUser.save();

    res.status(201).send({
      user: savedUser.toObject()
    })
    
  } catch (e) {
    res.status(500).send({
      error: true,
      errors:[{
        type: 'GLOBAL_ERROR',
        field: '', // no specific field
        code: 'REGISTRATION_ERROR',
        message: 'An error occured during registration'
      }]
    })
  }
}
