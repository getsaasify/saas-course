const mongoose = require('mongoose');
mongoose.Promise = Promise;

function connectToDB() {
  mongoose.connect(process.env.MONGODB_URI)
    .then((results) => {
      console.log('Success: Connected to MONGODB');
    })
    .catch((error) => {
      console.log('Error: Could not connect to MONGODB');
    });
}

module.exports = {
  connectToDB
}