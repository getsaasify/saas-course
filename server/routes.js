const authController = require('./controllers/authController');

function addRoutes(app) {
  app.all('*', (req, res, next) => {
    console.log(req.method + ' ' + req.url);
    next();
  });

  app.post('/api/register', authController.register);
}

module.exports = {
  addRoutes
}