const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');


const userSchema = new Schema({
  email: { type: String, unique: true, required: true, lowercase: true },
  password: { type: String, required: true }
},
{
  timestamps: true
})

userSchema.pre('save', function(next) {
  const user = this;

  if (!user.isModified('password')) {
    return next();
  }

  // use bcrypt to encrypt
  bcrypt.genSalt(5, function (err, salt) {
    if (err) {
      return next(err);
    }

    bcrypt.hash(user.password, salt, function(err, hash) {
      if (err) {
        return next(err);
      }

      user.password = hash;
      next();
    })
  }) 
})

userSchema.methods.comparePassword = function(candidatePassword, callback) {
  const user = thisl
  bcrypt.compare(candidatePassword, user.password, function(err, isMatch) {
    if (err) {
      return callback(err);
    }

    callback(err, isMatch);
  })
}

const User = mongoose.model('User', userSchema);

module.exports = User;
