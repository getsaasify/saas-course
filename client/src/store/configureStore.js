import { createStore } from 'redux';
import rootReducer from '../reducers/rootReducer';

let store;

const configureStore = () => {
  // middlewares such as redux-logger, redux-thunk
  // dev tools


  store = createStore(rootReducer);
}


if (!store) {
  configureStore();
}



export default store;