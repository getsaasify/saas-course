import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import Routes from './routes';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';

import store from './store/configureStore';
const history = createBrowserHistory();

ReactDOM.render(
  <Provider store={store} history={history}>
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
  </Provider>

  , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
