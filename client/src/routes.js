import React from 'react';
import App from './App';
import { Route } from 'react-router-dom';
import Register from './components/accounts/Register';
import RegisterV2 from './components/accounts/RegisterV2';

export default () => {
  return (
    <App>
      <Route path="/register" component={Register} />
      <Route path="/registerv2" component={RegisterV2} />
    </App>
  )
}