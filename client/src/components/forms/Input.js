import React, { Component } from 'react';

class Input extends Component {
  render() {
    const {
      input,
      meta: {
        touched,
        active,
        error
      },

      label,
      type,
      placeholder,
      helpText
    } = this.props;

    const showError = !active && touched && error;

    return (
      <div className="form-group">
        <label htmlFor="email">{label}</label>
        <input
          type={type}
          className={`form-control ${showError ? 'is-invalid' : ''}`}
          placeholder={placeholder}
          {...input}
        />
        {
          showError &&
          <div className="invalid-feedback">{error}</div>
        }

        <small classname="form-text text-muted">{helpText}</small>
      </div>
    );
  }
}
export default Input;
