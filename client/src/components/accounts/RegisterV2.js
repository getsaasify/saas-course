import React, { Component } from 'react';
import Input from '../forms/Input';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { register } from './accountsApi';
import _get from 'lodash/get';
import _find from 'lodash/find';

class RegisterV2 extends Component {

  state = {
    submitSucceeded: false
  }

  submitRegisterForm = (values) => {
    const {
      email,
      password
    } = values;

    return register(email, password)
      .then((results) => {
        console.log('results', results);

        this.setState({
          submitSucceeded: true
        });

        this.props.reset();
      })
      .catch((error) => {
        console.log('error ', error);

        const errors = _get(error, 'errors')

        const emailError = _find(errors, { field: 'email' }) || {};
        const passwordError = _find(errors, { field: 'password' }) || {};
        const globalError = _find(errors, { type: 'GLOBAL_ERROR' }) || {};

        throw new SubmissionError({
          email: emailError.message,
          password: passwordError.message,
          _error: globalError.message

        })
      });
  }

  render() {
    const { error } = this.props;

    return (
      <div>
        <h3>RegisterV2</h3>
        {
          error &&
          <div className="alert alert-danger">
            {error}
          </div>
        }

        {
          this.state.submitSucceeded &&
          <div className="alert alert-success">
            You have successfully registered
          </div>
        }

        <form onSubmit={this.props.handleSubmit(this.submitRegisterForm)}>
          <Field
            name="email"
            component={Input}
            helpText="Please enter your email address"
            placeholder="Email@domain.com"
            type="text"
          />

          <Field
            name="password"
            component={Input}
            helpText="Please enter your password"
            placeholder=""
            type="password"
          />

        
          <button
            type="submit"
            className="btn btn-primary"
          >Register</button>
        </form>
      </div>
    );
  }
}

const registerFormValidator = (values) => {
  const errors = {};

  if (!values.email) {
    errors.email = 'Email is required';
  }

  if (!values.password) {
    errors.password = 'Passowrd is required';
  }

  return errors;

}

let RegisterForm = reduxForm({
  form: 'REGISTER',
  validate: registerFormValidator
})(RegisterV2)

export default RegisterForm;


