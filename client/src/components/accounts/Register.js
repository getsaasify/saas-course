import React, { Component } from 'react';
import { register } from './accountsApi';

class Register extends Component {
  state = {
    email: '',
    password: '',
    submitSucceeded: false
  }
  handleChange = (event) => {
    const {
      name,
      value
    } = event.target;

    this.setState({
      [name]: value
    })
  }

  handleSubmit = () => {
    // Send data to server

    this.setState({
      submitSucceeded: false
    })

    const {
      email,
      password
    } = this.state;

    return register(email, password)
      .then((results) => {
        console.log('results', results);

        this.setState({
          submitSucceeded: true
        })
      })
      .catch((error) => {
        console.log('error ', error);
      });
  }

  render() {
    return (
      <div className="container">
        <h3>Register</h3>
        {
          this.state.submitSucceeded &&
          <div className="alert alert-success">
            You have successfully registered
          </div>
        }

        <form>
          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input
              name="email"
              type="email"
              className="form-control"
              id="email"
              aria-describedby="emailHelp"
              placeholder="Enter email"
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              name="password"
              type="password"
              className="form-control"
              id="password"
              placeholder="Password"
              onChange={this.handleChange}
            />
          </div>
  
          <button
            type="button"
            className="btn btn-primary"
            onClick={this.handleSubmit}
          >Register</button>
        </form>
      </div>
    );
  }
}
export default Register;
