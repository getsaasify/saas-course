export function register(email, password) {
  const url = '/api/register';

  return fetch(url, {
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    credentials: 'same-origin',
    method: 'POST',
    body: JSON.stringify({
      email,
      password
    })
  })
  .then((response) => {
    if (!response.ok) {
      return response.json().then((err) => { throw err })
    }
    
    return response.json();
  })
  .catch((error) => {
    throw error;
  });
}